### Overview

Kimai is a free & open source timetracker. It tracks work time and prints out a summary of your activities on demand. Yearly, monthly, daily, by customer, by project … It’s simplicity is its strength. Due to Kimai’s browser based interface it runs cross-platform, even on your mobile device.

### Feature list

* Mobile ready
* Multi user
* Multi language
* Teams
* Projects
* Activities
* AM/PM format
* Timesheet filter
* Weekend rates
* Archiving
* Permissions
* User profile
* Theming
* User registration

