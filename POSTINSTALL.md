This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin<br/>
**Password**: changeme123<br/>

Please change the admin password and email immediately.

<sso>

Login as the admin user to grant admin previleges to Cloudron users.

</sso>
