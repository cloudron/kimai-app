FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code
WORKDIR /app/code

# renovate: datasource=github-releases depName=kimai/kimai versioning=semver
ARG KIMAI_VERSION=2.30.0

# download release code
RUN curl -L https://github.com/kimai/kimai/archive/${KIMAI_VERSION}.tar.gz | tar -xz --strip-components 1 -f - -C /app/code

RUN chown www-data:www-data -R /app/code

RUN sudo -u www-data composer install --no-interaction --optimize-autoloader --no-dev & \
    sudo -u www-data composer require laminas/laminas-ldap --optimize-autoloader

# these links will become valid after setup is run
# export is from https://www.kimai.org/documentation/export.html#adding-export-templates
RUN rm -rf /app/code/.env                   && ln -s /app/data/env             /app/code/.env                  && \
    rm -rf /app/code/config/packages/cloudron.yaml   && ln -s /run/kimai/cloudron.yaml   /app/code/config/packages/cloudron.yaml  && \
    rm -rf /app/code/config/packages/local.yaml      && ln -s /app/data/local.yaml       /app/code/config/packages/local.yaml     && \
    rm -rf /app/code/var/sessions           && ln -s /run/kimai/sessions        /app/code/var/sessions          && \
    rm -rf /app/code/var/log                && ln -s /run/kimai/log             /app/code/var/log               && \
    rm -rf /app/code/var/cache              && ln -s /run/kimai/cache           /app/code/var/cache             && \
    rm -rf /app/code/var/data               && ln -s /app/data/data             /app/code/var/data              && \
    rm -rf /app/code/var/export             && ln -s /app/data/export           /app/code/var/export            && \
    rm -rf /app/code/var/invoices           && ln -s /app/data/invoices         /app/code/var/invoices          && \
    rm -rf /app/code/public/avatars         && ln -s /app/data/avatars          /app/code/public/avatars        && \
    rm -rf /app/code/var/plugins            && ln -s /app/data/plugins          /app/code/var/plugins

RUN chown www-data:www-data -R /app/code

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
RUN a2enmod rewrite
ADD apache/kimai.conf /etc/apache2/sites-enabled/kimai.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# configure php. kimai stores php sessions in the database
RUN crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_filesize 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP post_max_size 64M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP memory_limit 128M && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.save_path /run/php/sessions && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_maxlifetime 2592000 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_divisor 100

RUN ln -s /app/data/php.ini /etc/php/8.1/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.1/cli/conf.d/99-cloudron.ini

COPY start.sh env.template cloudron.yaml.template /app/pkg/

CMD [ "/app/pkg/start.sh" ]
