#!/usr/bin/env node

/* jshint esversion: 8 */
/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    { Builder, By, until, Key } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

if (!process.env.USERNAME || !process.env.PASSWORD) {
    console.log('USERNAME and PASSWORD env vars need to be set');
    process.exit(1);
}

describe('Application life cycle test', function () {
    this.timeout(0);

    const EXEC_OPTS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = parseInt(process.env.TIMEOUT, 10) || 10000;
    const USERNAME = process.env.USERNAME;
    const PASSWORD = process.env.PASSWORD;
    const ADMIN_USERNAME = 'admin';
    const ADMIN_PASSWORD = 'changeme123';
    const CUSTOMER_NAME = 'Herbert Burgermeister';

    let app;
    let browser;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function firstTimeLogin(username, password) {
        await browser.get(`https://${app.fqdn}/en_US/login`);
        await waitForElement(By.xpath('//input[@name="_username"]'));
        await browser.findElement(By.xpath('//input[@name="_username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//input[@name="_password"]')).sendKeys(password);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        await browser.sleep(3000);
        await waitForElement(By.xpath('//h1[text()="Welcome to Kimai"]'));
        await waitForElement(By.xpath('//a[contains(@href, "/wizard/profile")]'));
        await browser.findElement(By.xpath('//a[contains(@href,"/wizard/profile")]')).click();
        await waitForElement(By.id('form_language'));
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        await waitForElement(By.xpath('//h1[text()="Congratulations"]'));
        await waitForElement(By.xpath('//a[contains(@href,"/homepage")]'));
        await browser.findElement(By.xpath('//a[contains(@href,"/homepage")]')).click();
        await waitForElement(By.xpath('//span[text()="Dashboard"]'));
    }

    async function login(username, password) {
        await browser.get(`https://${app.fqdn}/en_US/login`);
        await waitForElement(By.xpath('//*[@name="_username"]'));
        await browser.findElement(By.xpath('//*[@name="_username"]')).sendKeys(username);
        await browser.findElement(By.xpath('//*[@name="_password"]')).sendKeys(password);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        await waitForElement(By.xpath('//span[text()="Dashboard"]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);
        await browser.sleep(2000);
        const srcLink = await browser.findElement(By.xpath('//a[contains(@href, "/logout")]')).getAttribute('href');
        console.log(srcLink);
        await browser.get(srcLink);
        await browser.sleep(2000);
    }

    async function addCustomer() {
        await browser.get(`https://${app.fqdn}/en/admin/customer/create`);
        await waitForElement(By.id('customer_edit_form_name'));
        await browser.findElement(By.id('customer_edit_form_name')).sendKeys(CUSTOMER_NAME);
        await browser.findElement(By.id('customer_edit_form_name')).sendKeys(Key.ENTER);
        await waitForElement(By.xpath(`//span[contains(., "${CUSTOMER_NAME}")]`));
    }

    async function getCustomer() {
        await browser.get(`https://${app.fqdn}/en/admin/customer/1/details`);
        await waitForElement(By.xpath(`//span[contains(., "${CUSTOMER_NAME}")]`));
    }

    // install
    it('install app', () => execSync(`cloudron install --location ${LOCATION}`, EXEC_OPTS));
    it('can get app information', getAppInfo);
    it('can first time login', firstTimeLogin.bind(null, USERNAME, PASSWORD));
    it('can logout', logout);
    it('can first login as admin', firstTimeLogin.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can add customer', addCustomer);
    it('can get customer', getCustomer);
    it('can logout', logout);

    // restart
    it('can restart app', () => execSync(`cloudron restart --app ${app.id}`, EXEC_OPTS));
    it('can login', login.bind(null, USERNAME, PASSWORD));
    it('can logout', logout);
    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can get customer', getCustomer);
    it('can logout', logout);

    // backup/restore
    it('backup app', () => execSync(`cloudron backup create --app ${app.id}`, EXEC_OPTS));
    it('restore app', () => {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_OPTS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_OPTS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_OPTS);
    });
    it('can login', login.bind(null, USERNAME, PASSWORD));
    it('can logout', logout);
    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can get customer', getCustomer);
    it('can logout', logout);

    // reconfigure
    it('move to different location', async function () {
        browser.manage().deleteAllCookies();

        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');

        execSync(`cloudron configure --app ${app.id} --location ${LOCATION}2`, EXEC_OPTS);

        getAppInfo();
    });
    it('can login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can logout', logout);
    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can get customer', getCustomer);

    // uninstall
    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');

        execSync(`cloudron uninstall --app ${app.id}`, EXEC_OPTS);
    });

    // test update
    it('can install app', () => execSync(`cloudron install --appstore-id org.kimai.cloudronapp --location ${LOCATION}`, EXEC_OPTS));

    it('can get app information', getAppInfo);
    it('can first time login', firstTimeLogin.bind(null, USERNAME, PASSWORD));
    it('can logout', logout);
    it('can first time login as admin', firstTimeLogin.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can add customer', addCustomer);
    it('can get customer', getCustomer);
    it('can logout', logout);

    it('can update', () => execSync(`cloudron update --app ${LOCATION}`, EXEC_OPTS));

    it('can admin login', login.bind(null, ADMIN_USERNAME, ADMIN_PASSWORD));
    it('can get customer', getCustomer);
    it('can logout', logout);
    it('can login', login.bind(null, USERNAME, PASSWORD));
    it('can logout', logout);

    it('uninstall app', async function () {
       // ensure we don't hit NXDOMAIN in the mean time
       browser.get('about:blank');

       execSync(`cloudron uninstall --app ${app.id}`, EXEC_OPTS);
    });
});
