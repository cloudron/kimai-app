#!/bin/bash

set -eu -o pipefail

readonly mysql="mysql --user=${CLOUDRON_MYSQL_USERNAME} --host=${CLOUDRON_MYSQL_HOST} --password=${CLOUDRON_MYSQL_PASSWORD} --port=${CLOUDRON_MYSQL_PORT} ${CLOUDRON_MYSQL_DATABASE}"

echo "=> Ensure directories"
mkdir -p /run/php/sessions /run/kimai/sessions /run/kimai/cache /app/data/data /run/kimai/log /app/data/plugins /app/data/avatars /app/data/invoices

if [[ ! -f /app/data/local.yaml ]]; then
    echo -e "# See https://www.kimai.org/documentation/local-yaml.html\n\n" > /app/data/local.yaml
fi

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

if [[ ! -f /app/data/env ]]; then
    cp /app/pkg/env.template /app/data/env
    sed -e "s,APP_SECRET=.*,APP_SECRET=$(pwgen -1s 32)," -i /app/data/env
fi

sed -e "s,TRUSTED_PROXIES=.*,TRUSTED_PROXIES=${CLOUDRON_PROXY_IP}," \
    -e "s,DATABASE_URL=.*,DATABASE_URL=${CLOUDRON_MYSQL_URL}?serverVersion=8.0," \
    -e "s,MAILER_FROM=.*,MAILER_FROM=${CLOUDRON_MAIL_FROM}," \
    -e "s,MAILER_URL=.*,MAILER_URL=smtp://${CLOUDRON_MAIL_SMTP_USERNAME}:${CLOUDRON_MAIL_SMTP_PASSWORD}@${CLOUDRON_MAIL_SMTP_SERVER}:${CLOUDRON_MAIL_SMTP_PORT}?encryption=\&auth_mode=," \
    -i /app/data/env

echo "=> Create cloudron.yaml"
sed -e "s,CLOUDRON_LDAP_URL,${CLOUDRON_LDAP_URL}," \
    -e "s/CLOUDRON_LDAP_PORT/${CLOUDRON_LDAP_PORT}/" \
    -e "s/CLOUDRON_LDAP_BIND_DN/${CLOUDRON_LDAP_BIND_DN}/" \
    -e "s/CLOUDRON_LDAP_BIND_PASSWORD/${CLOUDRON_LDAP_BIND_PASSWORD}/" \
    -e "s/CLOUDRON_LDAP_USERS_BASE_DN/${CLOUDRON_LDAP_USERS_BASE_DN}/" \
    /app/pkg/cloudron.yaml.template > /run/kimai/cloudron.yaml

echo "=> Ensure permissions"
chown -R www-data.www-data /app/data /run

echo "=> Run installer"
sudo -u www-data bin/console kimai:install -n

echo "=> Ensure admin account"
count=`$mysql --skip-column-names -s -e "SELECT COUNT(*) FROM kimai2_users WHERE username='admin';"`
if [[ "$count" = "0" ]]; then
    echo "==> Create admin account"
    sudo -u www-data bin/console kimai:user:create admin admin@example.com ROLE_SUPER_ADMIN changeme123
fi

echo "=> Tailing app logs"
tail -F /run/kimai/log/prod.log &

echo "=> Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
