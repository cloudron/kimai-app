[1.0.0]
* Initial version

[1.1.0]
* Update Kimai to 1.2
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.2)
* Hide inactive users #1047
* Format ${query.month} as a number #1025
* Change/Set Currency Globally for Customers, Users, etc. #1017
* auto copy current values if autocopy flag is set for fields created though metafields #1015
* API [POST] User #977
* Minor: Currency upon NewProject #972
* Minor: Currency upon NewProject #972
* Better visualization of active time-recordings and start-stop #961
* Template variable with decimal time entry (with "dirty" self-made solutions) #955
* Configurable default date-range for initial filter in export, invoice and team-timesheets #887
* Exporting - Minutes output or a Summery of hours when filtering #882
* added API functions to save meta fields #1063 (kevinpapst)
* improve invoice handling for different rates #1058 (kevinpapst)
* support creating new roles #1050 (kevinpapst)
* refactored invoice and export forms #1046 (kevinpapst)

[1.2.0]
* Update Kimai to 1.3
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.3)
* ADDED customer specific translations #1099 (kevinpapst)
* ADDED support multiple invoice repositories #1084 (kevinpapst)
* ADDED font-awesome paper-plane #1077 (hmr-it-jr)
* FIXED swagger file type text #1094
* FIXED Cookie Expiration should automatically lead to sign in page #1027
* FIXED Tags search fails when there are numerous tags which start with a specific identifier #1082
* FIXED Content of ${entry.description} not from table timesheet.description #1076
* FIXED Wrong ${entry.total} for fixed rate items if more than one #1073
* FIXED When create invoice, Description field is not description but is Activity #1093
* FIXED Critical error after upgrading from 1.1 to 1.2 #1071

[1.3.0]
* Update Kimai to 1.4
* User view to list the teams names where the user is assigned as a user and as a team lead #1146
* custom fields - User Specific instructions or help fields for end user entry guidance #1133
* Provide full usage of CustomMetaFields #1103
* Profile image #1078
* Composer update #1104 (kevinpapst)
* added czech translations #1075 (kevinpapst)
* upgrade difficulties #1153
* Forgetting duration on timesheet entry gives error 500 and save remains greyed out #1147
* Invoice - Print and preview results are not the same when no users are selected #1114
* Prevent search from getting closed on each selection #1112
* Using the variable ${entry.duration_decimal} and ${invoice.duration_decimal} should always have 2 decimals #1080
* fix migrations for older database versions #1148 (kevinpapst)
* Post 1.3 #1106 (kevinpapst)

[1.3.1]
* Update Kimai to 1.4.1
* fix missing translation for preset datatable headers (only for dev env)
* better dependency check for avatar service (see tobybatch/kimai2#32)
* show only own teams to teamleads (#1157)

[1.3.2]
* Update Kimai to 1.4.2
* Fix dashboard widget type
* Bump version number
* Use prod as default APP_ENV (#1166)
* Remove deprecated tooltip without translation

[1.4.0]
* Update Kimai to 1.5
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.5)
* Query API by custom fields / meta fields #1174
* Add a "now" button to manual timesheet entry #844
* dynamic branding options #1205 (kevinpapst)
* reformat edit forms #1192 (kevinpapst)
* improve list views #1191 (kevinpapst)
* added order-date field to project #1186 (kevinpapst)
* Added some spanish new strings #1183 (yayitazale)
* duration field in timesheet edit dialog #1180 (kevinpapst)
* Turkish Language Translations #1160 (guneysus)

[1.5.0]
* Update Kimai to 1.6
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.6)

[1.5.1]
* Update Kimai to 1.6.1
* Fix installer for older databases and improve invoice templates #1282 (kevinpapst)
* Fix sqlite installer #1283 (kevinpapst)

[1.5.2]
* Make avatar folder writeable

[1.5.3]
* Update Kimai to 1.6.2
* Currency symbol (like €) to be shown with rates on invoice #1288
* Access to teams with API #1286
* Invoice not really usable #1237
* Danish translations #1296 (Badgie)
* Error thrown while trying to restart a job using the 'Start again' option in the 'Actions' menu #1295
* Including the active/inactive flag in the dashboard tiles #1221
* Dashboard shows numbers of project without having access to it #1161

[1.6.0]
* Update Kimai to 1.7
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.7)

[1.6.1]
* Update Kimai to 1.8
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.8)

[1.7.0]
* Update Kimai to 1.9
* Update to new Cloudron base image
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.9)

[1.8.0]
* Update Kimai to 1.10
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.10)
* Adding/viewing Acitivities while in Project view #1885
* Report, copy&paste in Excel issue #1839
* reporting feature: summary row in first line; last column always visible; fixed width of first column #1824
* Add username ID to timesheet export #1823
* Export user list #1822
* Only question: Export of all projects even if they are empty #1819
* Update manifest screenshots
* Make php.ini customizable

[1.8.1]
* Update Kimai to 1.10.2
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.10.2)
* Added support for ordering meta fields #1940 (kevinpapst)
* Fixed: money formatting issues for money without currency #1932 (kevinpapst)
* Fixed: On the dashboard the revenue widget isn't working. #1923
* Update italian translations (1.10) #1929 (buoncri)

[1.8.2]
* Extend session expiry to 1 month. See this [issue](https://github.com/kevinpapst/kimai2/issues/1950)

[1.9.0]
* Update Kimai to 1.11
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.11)

[1.9.1]
* Update Kimai to 1.11.1
* validate color #2072
* fix assertion deprecation #2068
* Translations update from Weblate #2066
* Can create second timesheet entry #2064
* fix auto-stop when starting timesheet with tags #2067

[1.10.0]
* Update Kimai to 1.12
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.12)
* Consolidation of items in the search function between time recording and expenses #2118
* Dashboard > My Teams > displayed without order #1773
* allow custom export repositories #2182 (kevinpapst)
* helper function for invoice templates #2168 (kevinpapst)
* allow to reassign timesheet from delete user dialog #2159 (kevinpapst)
* sort teams alphabetically in team_list #2119 (kevinpapst)
* added event to manage javascript translations #2104 (kevinpapst)
* sort composer packages #2089 (kevinpapst)
* Api doc #2085 (kevinpapst)
* updated dependencies #2125 (kevinpapst)
* display application name and version in console #2150 (kevinpapst)
* escape configurable fields #2191, #2192 (kevinpapst)
* Translations update from Weblate #2081, #2071, #2106, #2136, #2135, #2102, #2093 (weblate)

[1.10.1]
* Update Kimai to 1.13
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.13)

[1.11.0]
* Update base image to v3
* Update PHP to 7.4

[1.12.0]
* enable sendmail addon
* configure trust proxy
* generate app secret

[1.13.0]
* Update Kimai to 1.14
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.14)
* Tags in invoices #2525
* Monthly view for all users #2460
* Invoices -> new field to save the date when the invoice was payed #2449
* More columns for the spreadsheet exporter #2355
* Projects: Make data field "time budget" available in "eye" function of list view #2343
* Reporting: Not only for users, also for projects #2265
* Calendar: add day-total per day on the calendar-view #2252
* Batch generation of invoices for all customers #2178

[1.13.1]
* Update Kimai to 1.14.1
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.14.1)

[1.13.2]
* Update Kimai to 1.14.3
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.14.3)

[1.13.3]
* Fix invoice template uploads

[1.14.0]
* Update Kimai to 1.15.0
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.15)

[1.14.1]
* Update Kimai to 1.15.1
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.15.1)

[1.14.2]
* Update Kimai to 1.15.2
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.15.2)
* Copy the billable status when restarting timesheet #2778 (Guillaume-Duc-95)
* Translations update from Weblate #2781 (weblate)

[1.14.3]
* Update Kimai to 1.15.3
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.15.3)
* export budget and timeBudget #2812 (kevinpapst)
* removed subtitle #2811 (kevinpapst)
* Translations update from Weblate (#2791) @milotype @dansamara @bittin

[1.14.4]
* Update Kimai to 1.15.4
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.15.4)
* added croatian language #2817 (kevinpapst) - thanks @milotype
* set security options on cookies #2825 (kevinpapst) - thanks @nareshsarnala
* fix open budget calculation #2821 (kevinpapst) - thanks @diamondq

[1.14.5]
* Update Kimai to 1.15.5
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.15.5)
* In create screen make option to make Billable status to not billable #2779
* Make billable field batch editable #2694
* Timesheet uncheck billable by default #2625
* API: Add customer number to customer collection #2358
* improve billable flag #2851 (kevinpapst)
* refactor system configuration title and wording #2847 (kevinpapst)
* Translations update from Weblate #2823 (weblate)

[1.14.6]
* Update Kimai to 1.15.6
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.15.6)
* improve summary rows layout in reports #2861 (kevinpapst)
* improve sunday FDOW handling #2862 (kevinpapst)
* make sure that minute_increment is not zero #2860 (kevinpapst)

[1.15.0]
* Update Kimai to 1.16
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.16)

[1.15.1]
* Update Kimai to 1.16.1
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.16.1)

[1.15.2]
* Update Kimai to 1.16.2
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.16.2)
* include calendar week in week chooser #2933
* csrf tokens for multiple actions - thanks @haxatron

[1.15.3]
* Update Kimai to 1.16.3
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.16.3)
* CSRF Tokens are not properly refreshed on some form submissions #2947 #2948 - thanks @tdozbun-reno
* escape customer, project and activity name in javascript (#2959)
* escape data in calendar popover (#2960)
* make sure that markdown uses safe mode (#2961)
* improve permissison handling in invoice screen (#2965)

[1.15.4]
* Update Kimai to 1.16.4
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.16.4)
* allow to delete invoice documents from within kimai (#2968)

[1.15.5]
* Update Kimai to 1.16.5
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.16.5)
* Fix version number

[1.15.6]
* Update Kimai to 1.16.6
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.16.6)
* fix HTML forms (two opening tags) #2972
* fix deleting invoice documents #2980

[1.15.7]
* Update Kimai to 1.16.8
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.16.8)
* new invoice template variables for budgets #3005
* new command to work with translation files #2993
* cleanup duplicate translation ids #3001
* improve translation test #2998
* prevent that lock files will be committed in PRs #2983 and #2992
* translations update from Weblate #2951
* fix selection of customer/project/activity in multi-step forms #2989
* more csrf protection for invoice and search #2984 - thanks @Haxatron
* fix invoice create and search #2990
* Fix invoice creation issue

[1.15.8]
* Update Kimai to 1.16.9
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.16.9)
* Translations update from Weblate #3013
* Fix projects are not filtered after submit #3016
* Fix invoice budget calculation #3024
* Fix "filter user timesheet" action display #3018

[1.15.9]
* Update Kimai to 1.16.10
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.16.10)
* weekly hours form: allow to configure the amount of recent activity rows in an empty week #3026
* added comment field to invoice #3045
* improve export permission checks #3027
* Translations update from Hosted Weblate #3032

[1.15.10]
* Fix issue where PHP memory limit was hardcoded in apache

[1.16.0]
* Update Kimai to 1.17.0
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.17)
* bump dependencies #3089 (kevinpapst)
* code improvements #3088 (kevinpapst)
* improve permission handling for quick entry controller #3081 (kevinpapst)
* Invoice events #3079 (kevinpapst)
* Bugfixes #3078 (kevinpapst)
* Invoice meta fields #3077 (kevinpapst)
* shrink prod error messages #3091 (kevinpapst)
* phpstan improvements #3092 (kevinpapst)
* new export template #3082 (kevinpapst)
* Translations update from Hosted Weblate #3048 (weblate)
* Import from Kimai v1: This timesheet is already exported #3061

[1.16.1]
* Update Kimai to 1.17.1
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.17.1)
* Translations update from Hosted Weblate (#3093)
* Added invoice delete event (#3096)
* Fix admin access for customer in invoice module (#3095)
* Improve team member handling (#3097)
* Can't add user to existing team (#2921)
* [API] PATCH team members removes existing ones (#2975)

[1.16.2]
* Update Kimai to 1.18
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.18)
* Reporting: choose data-types and new user per year #3155 (kevinpapst)
* Reporting: export user-list reports in excel #3154 (kevinpapst)
* Configure display of customer, project and activity in dropdown lists #3151 (kevinpapst)
* Translations update from Hosted Weblate #3105, #3174, #3171, #3160
* allow to change password interactively on the console (prevents bash history leaks)
* Decimal format in print export doesn't work when language is set to German (Austria) #3172
* xlsx export: change SUM function to SUBTOTAL function - fix #3165 #3166 (fredyb)
* fix negative sum display in excel export
* use proper cell format for report exports #3178 (kevinpapst)
* Missing translation files #3173

[1.16.3]
* Update Kimai to 1.18.1
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.18.1)
* Activated language Persian
* Added and activated language Norwegian Bokmål - thanks @comradekingu
* Added comment to customer/project/activity entity and collections API
* Fix: translation file extensions / activate missing translations in de_CH, pt_BR, zh_CN
* Fix: title pattern when description is included in customer/project/activity dropdown

[1.16.4]
* Update Kimai to 1.18.2
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.18.2)
* PDF invoice templates #3190
* Sort users by display-name in users reports #3190
* Improve console version output #3190
* Translations using Weblate #3181
* Fix: fix select2 and dropdown width for quick-entry form #3188
* Fix: fix empty string issue in csv export- fixes #3189
* Fix: deprecations with php 8 (twig with sort filter) #3190
* Fix: missing custom translations in modal dialogs #3190

[1.17.0]
* Update Kimai to 1.19
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.19)
* Added event to extend the detail pages from plugins #3209
* Default option of the "Billable" attribute for each defined activity #3200, #2594
* Kimai API billable switch missing #2705
* Translations update from Hosted Weblate #3208, #3194
* Better default button label for non-translated renderer #3204
* Avatar size differences for image URL / no image URL set #3180
* SVG avatars not shown in top bar #3163
* defensive javascript #3210
* Fixed truncated comments: customer, project, activity, task #3204
* Fixed avatar image size when using images #3204, #3163, #3180

[1.17.1]
* Update Kimai to 1.19.1
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.19.1)
* support more complex metafield queries #3228 (kevinpapst)
* show user account number in report export #3224 (kevinpapst)
* Translations update from Hosted Weblate #3221 (weblate)
* Billable of time-records created from recent acitivities in calendar view #3218
* fix billable calculation on timesheet restart #3225 (kevinpapst)
* fix relative times in budget calculation in export #3216 (kevinpapst)

[1.17.2]
* Update Kimai to 1.19.3
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.19.3)
* allow arbitrary string length for system configurations #3243
* Translated using Weblate #3235
* improved logic to extract configuration from string #3244

[1.17.3]
* Update Kimai to 1.19.4
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.19.4)
* Login redirects to homepage if already being logged-in #3255
* Added new report: month grouped by project, activity and user #3255
* Translations update from Hosted Weblate #3263
* Fix budget validation for entries that are moved to another moth #3255
* Invoice: fix amount should be decimal if decimal template is used #3255

[1.17.4]
* Update Kimai to 1.19.5
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.19.5)
* Allow changing calendar entry title #3272
* Added sort field for export items #3265
* Trigger event if multiple invoices were created #3265
* Provide entries as variables in twig invoice templates #3265
* Added symfony-cli to check for security issues in packages #3265
* Translations update from Hosted Weblate #3267 - thanks @weblate @shinkuroshi @xiexieqing

[1.17.5]
* Update Kimai to 1.19.6
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.19.6)
* fix saving in quick-entry form with empty rows
* fix totals cell alignment in new customer report
* reactivate phpstan bleeding edge rules after update

[1.17.6]
* Update Kimai to 1.19.7
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.19.7)
* re-use the pattern for optgroup titles (e.g. project titles in activity dropdown)
* update all dependencies
* pre-select an option if it is the only available one (projects and activities)
* added command to stop all active timesheets

[1.18.0]
* Update Kimai to 1.20.0
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.20)
* Allow switching user displayed in calendar #3314, #1746, #1067
* Permission check for "mark-as-exported" checkboxes #3313
* Prevent bookings with same start / stop time #3304, #2342
* Added weekly-hours setting "how many weeks in the past for recent activities" #3296
* Added weekly-hours setting "minimum number of rows" #3297
* Added en_GB locale with dd/mm/yyyy format #3311
* Translated using Weblate #3310, #3276

[1.18.1]
* Update Kimai to 1.20.1
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.20.1)
* improved timesheet calculator with changesets and priority #3317
* helper method to reset timesheet rates #3317
* calculate and include exported stats (e.g. available in export templates) #3317
* added hourly rate column to timesheet listing #3317

[1.18.2]
* Update Kimai to 1.20.2
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.20.2)
* added invoice text field to project and activity #3335
* added css class for custom fields columns on listing page #3328, #3336
* added css classes in many forms for simpler customization via custom css #3320
* Translated using Weblate #3321, #3329

[1.18.3]
* Update Kimai to 1.20.3
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.20.3)
* Added new permission to separate time and money budget #3352
* Support pdfContext for PDF invoice templates #3340
* Hide user switcher in calendar if there is only one user to choose #3340
* Mark invoices as exported by default #3340
* Make quick entry responsive for mobile-only users #3340
* Updated composer packages #3340
* Translations update from Weblate #3339

[1.18.4]
* Update Kimai to 1.20.4
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.20.4)
* fix calendar drag and drop for regular user #3358

[1.18.5]
* symlink public/bundles directory

[1.19.0]
* Update Kimai to 1.21.0
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.21.0)
* Budget graph in project details (#3406)
* Translations update from Hosted Weblate (#3399)
* allow to filter for canceled invoices (#3415)
* re-style overlapping border (#3400)
* new command to delete empty translations (#3392)
* Translations update from Hosted Weblate (#3356)
* Suppress deprecation notice, convert route param (#3391)
* require a language for invoice templates (#3387)
* allow to set api token when creating user via API (#3380)
* Export invoice metafields (#3366)
* Cleanup (#3363)

[1.20.0]
* Update Kimai to 1.22.0
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.22.0)
* Translations update from Hosted Weblate (#3436)
* rebuild assets for croatian (#3451)
* Croatian translation 20220730 (#3447)
* code improvements (#3438)
* show total hourly rate in detail pages (#3441)
* saml: allow to keep existing roles on login (#3440)
* allow to restrict usage of global activities for projects (#3437)
* link customer, project and activity in invoice listing (#3428)
* total sums for duration and rate in invoice and export preview (#3431)
* Project date-range report: allow budget-type independent project-listing (#3430)

[1.20.1]
* Update Kimai to 1.22.1
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.22.1)
* bump composer packages (#3455)
* fix invoice preview opening in current tab (#3454)

[1.21.0]
* Update Kimai to 1.23.0
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.23.0)
* Order quick entries by project name (#3488)
* Translations update from Hosted Weblate (#3462)

[1.21.1]
* Update Kimai to 1.23.1
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.23.1)
* fix unit test failing on month borders (#3494)
* Prevent invoice template update (#3493)

[1.22.0]
* Update Kimai to 1.24.0
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.24.0)
* Translations update from Hosted Weblate (#3496)
* Added inline disposition for PDF previews (#3486)
* Allow to hide zero tax rows via plugin (#3484)
* Support custom fonts in PDF via twig templates (#3509)
* fix datetime modify to now (#3511)
* fix quick entry during grace period matching midnight (#3504)

[1.23.0]
* Update Kimai to 1.25.0
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.25.0)
* Configurable calendar drag and drop behavior (#3537)
* Added keyboard shortcuts (#3536)
* Translations update from Hosted Weblate (#3523) (#3532) (#3542)
* Make project and customer available in export summaries (#3543)

[1.24.0]
* Update Kimai to 1.26.0
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.26.0)
* Added filter for globalActivities in project collection (#3565)
* Translations update from Hosted Weblate (#3549)
* Update composer packages (#3502)
* Project API and globalActivities flag (#3564)
* Use saml config interface instead of generic system configuration (#3551)

[1.25.0]
* Update Kimai to 1.27.0
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.27.0)
* Translations update from Hosted Weblate (#3570)
* do not show potential invoices with a negative total (#3579)
* allow negative duration via internal API (#3573)

[1.26.0]
* Update Kimai to 1.28.0
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.28.0)
* create exports via command (#3605)
* copy teams from logged-in user for new projects (#3599)
* added first and last date fields as invoice template variables (#3594)
* added team filter for invoice, export, reports (#3590)
* allow to show customer name in project dropdown (#3589)
* translations update from Hosted Weblate (#3586)

[1.26.1]
* Update Kimai to 1.28.1
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.28.1)
* bump dependencies and improved export command (#3606)

[1.27.0]
* Update Kimai to 1.29.1
* Update PHP to 8.1
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.29.0)
* Added project + customer setting for calendar entry titles (#3636)
* Added username parameter to export command (#3638)
* Translations update from Hosted Weblate (#3607) (#3628)

[1.28.0]
* Update Kimai to 1.30.0
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.30.0)
* Translations update from Hosted Weblate (#3655)
* Fix find activity with project in importer (#3681)
* Fix #3677 weekly timesheet uses automatic billable mode (#3679)
* Fixes: make table responsive (and remove duration dropdowns) only on screens < 1000px
* Exclude all 403 and 404 from logs
* Prevent mandatory user preferences turning null
* Cleanup bi-directional tag handling between timesheets and tags, causing issues for new tags

[1.28.1]
* Update Kimai to 1.30.1
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.30.1)
* Translations update from Hosted Weblate (#3705)
* changed repository url (#3708)

[1.28.2]
* Update Kimai to 1.30.2
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.30.2)
* Activated ukrainian

[1.28.3]
* Update Kimai to 1.30.3
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.30.3)
* fix calendar title for project - customer

[1.28.4]
* Update Kimai to 1.30.5
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.30.5)
* prevent empty invoice number
* prevent broken invoice filename
* deprecated API call leads to 500 error #3771
* allow to upload twig invoice templates via UI

[1.28.5]
* Update Kimai to 1.30.6
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.30.6)
* Fixed Call to a member function getId() on null in ActivityService (e.g. used in importer plugin)

[1.28.6]
* Update Kimai to 1.30.7
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.30.7)
* Fixed invalid version number
* Activated deprecation logs in development and test environment

[1.28.7]
* Update Kimai to 1.30.8
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.30.8)
* Fixed: weekly timesheet can delete valid records

[1.28.8]
* Update Kimai to 1.30.9
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.30.9)
* Fixed: allow to overwrite global spreadsheet styles for export renderer

[1.28.9]
* Update Kimai to 1.30.10
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/1.30.10)
* fixed replacer cannot find value for field (#3862)

[2.0.0]
* Update Kimai to 2.0.4
* **This is a major package release due to plugin incompatibility. Not all plugins may be compatible yet.**
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.4)
* Translated using Weblate (#3723)
* do not traverse into invoice template subdirectories (#3735)
* merged release 1.30.3 and 1.30.5 - allow to upload twig invoice templates via UI
* allow to pre-fill timesheet metafields via URL
* added test accounts with simpler names and password
* fix column bookmark cannot be saved (#3768)
* support adding existing teams with same name
* permissions cannot be set right after role was created (#3777)
* allow to deactivate unique customer number validation (#3762)
* invalid message when trying to edit locked or exported timesheets in calendar (#3766)
* updated icons and manifest (#3761)
* fix api description
* fix security open api definition
* Enabling daily_stats in User > Preferences breaks /timesheet (#3748)
* Plugin developers: read https://www.kimai.org/documentation/migration-v2.html
* Upgrade to new Symfony Security System - LDAP untested!
* Removed Sweetalert2 in favor of Bootstrap Toasts & Modals
* Removed gd extension requirement
* Removed third-party mailer packages
* Migrate to FullCalendar >= 5.10.2
* Migrated from Moment.js to Luxon
* Migrated from daterangepicker to litepicker (dependency free and without locale files)
* Removed jQuery
* Deleted settings for Random colors and limited colos (default true for both now)
* Deleted setting OFF for Minute selection for Duration
* Replaced all annotations (Symfony and Doctrine) with attributes
* Removed "invoice template" meta field support from invoice command (use template via customer!)
* added permission caching (#3877)
* removed duration_only mode
* cleanup database
* remove deleted user preference keys
* configure email validation mode to fix deprecation message
* allow to use non brand icon in saml provider
* new method getCalculatedDuration()
* getRawData() by id
* only stop entries if new one is running
* fix validator tampering with timesheet duration
* fix allow setting null as customer
* can be re-activated through kimai.yaml

[2.0.1]
* Update Kimai to 2.0.5
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.5)
* Update translation files

[2.0.2]
* Update Kimai to 2.0.6
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.6)
* cleanup invoice templates
* cleanup invoice translations
* fix invoice datetime for now
* support invoice archive order by number and status
* bump version
* allow meta-field sub-classing
* removed unused translation task.delete
* prevent null in `str_replace`

[2.0.3]
* Update Kimai to 2.0.7
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.7)

[2.0.4]
* Update Kimai to 2.0.8
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.8)
* support parsing negative durations in JS
* bump luxon dependency
* make sure that 2FA is not required for session based API calls
* show name of items to delete
* fix permission issue for recent activity items

[2.0.5]
* Update Kimai to 2.0.9
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.9)
* fix api definition
* include customer number in validation message
* fix doctrine deprecation, prepare for DBAL 4
* added DataSubscriberInterface to identify certain doctrine subscribers

[2.0.6]
* Update Kimai to 2.0.10
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.10)
* allow API calls via GET
* allow to stop timesheet via GET
* improve form handling and validation
* improve stop button handling
* bump version

[2.0.7]
* Update Kimai to 2.0.11
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.11)
* added "today" as selector in date-range dropdown
* added feature to prevent auto-select of dropdowns with only one entry
* added hint that no changes were detected in batch update
* added negative invoice sums are possible (e.g. for credit notes)
* fix project list is expanded after submission
* fix invalid date parsing causes 500
* fix: prevent auto-select of activities in export and invoice form (in case only one global activity exists)
* fix team assignments for customer and project were not saved (using API now)
* fix form fieldset with legend styling (e.g. team project assignment)
* fix required meta-field were forced to have a value in batch update
* fix tomselect meta-field was not disabled in batch update
* fix unset internal rate is shown as 0
* fix one minute rounding problem in duration-only mode  with "now" being default time
* fix column width and label for duration-only mode
* tech debt: cleanup invoice template (remove invoice layout)
* tech debt: reorder for simpler comparison with invoice form
* possible BC for devs: remove unused methods from form trait
* bump composer packages (includes new translations for auth screens)

[2.0.8]
* Update Kimai to 2.0.12
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.12)
* added submenus in action drodowns, to shorten them
* show all user-edit-screens in action dropdown
* fix cascade delete teams through customer
* fix cascade delete customer/project/activity through teams
* fix responsive classes for "internal rate" column
* clarify error message if invoice number generator or calculator is missing

[2.0.9]
* Update Kimai to 2.0.13
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.13)
* added missing escape to prevent HTML injection
* added missing color attribute
* upgrade theme
* use dropdown submenu if title is set, otherwise dropdown tends to get too long
* allow to use card-table instead of card-body
* added required attribute to username and password field
* fix pagination back to page 1
* prevent tag name too long
* re-add missing user preferences link

[2.0.10]
* Update Kimai to 2.0.14
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.14)
* show customer totals in single-user reports
* support custom fonts in export / invoice templates
* require daterange for export and invoice searches
* improve help label for user locale
* show decimal formats in help controller
* cache tag amount per request
* composer updates
* improved code styles
* added default label to date picker
* new macros & use own macro

[2.0.11]
* Update Kimai to 2.0.15
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.15)

[2.0.12]
* Update Kimai to 2.0.16
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.16)

[2.0.13]
* Update Kimai to 2.0.17
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.17)
* prevent empty title for select options
* removed css for unused sweetalert
* fix isWeekend() check with DateTimeInterface
* reset timesheet rates on "create copy"
* wrap long names in multi-selects - fix #4001
* rename profile menus

[2.0.14]
* Update Kimai to 2.0.18
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.18)
* prevent too long values for user preferences
* add chart value 0 to y-axis if last value > 0, to always display the zero line (project details)
* add user-preferences to invoice hydrator
* DateTime vs DateTimeInterface
* simplify permission config
* fix query for teamleads limiting to only selected teams not possible

[2.0.15]
* Update Kimai to 2.0.19
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.19)
* prevent invoices with exceeding filename
* fixed invalid LDAP log level
* support locale switching in action events

[2.0.16]
* Update Kimai to 2.0.20
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.20)
* max duration per entry increased from 8 to 10 hours
* fixes #3981 - clickable area in dropdown too small
* fixes #4008 - duplicate activities in project-details report
* headers and summary styling in project-details report
* show billable stats in project-details report
* added new invoice variable for entry.duration_format

[2.0.17]
* Update Kimai to 2.0.21
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.21)
* increase padding in wizard
* fix theme chooser in wizard
* improve dynamic export columns in PDFs
* added label for total column
* bump theme bundle
* improve avatar list spacing
* be more flexible parsing times - fixes #4031

[2.0.18]
* Update Kimai to 2.0.22
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.22)

[2.0.19]
* Update Kimai to 2.0.23
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.23)
* Dynamic favicon to indicate activity in pinned browser tabs (#4038)
* fix single dropdown became two lines (#4041)
* allow to remove date from non-required field (#4040)
* doctor: show available github release only if newer version exists (#4037)

[2.0.20]
* Update Kimai to 2.0.24
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.24)
* bump theme to get navigation id specific classes (#4065)
* added JS api to access current user in frontend (#4064)

[2.0.21]
* Update Kimai to 2.0.25
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.25)
* Export PDF: added support for hourly rate column in summary & detail table
* Display TOTP secret after clicking the QR code image - fixes #4006
* Developer: register your own icon via AbstractPluginExtension
* Developer: helper methods in CustomerService to reduce coupling to CustomerRepository
* Invoice document upload: fix validation for filenames with uppercase character - fixes #4073

[2.0.22]
* Update Kimai to 2.0.26
* [Full changelog](https://github.com/kevinpapst/kimai2/releases/tag/2.0.26)
* Modernized flat form styles (#4098)
* Use collapsible element instead of collapsible card (#4096)
* Support visibility for tags (#4086)
* Make setting "rounding days" optional (#4087)

[2.0.23]
* Update Kimai to 2.0.27
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.0.27)
* Improve UX for invoice template management (#4121)
* hide projects in "inactive projects" report, if they did not yet start (#4107)
* restyled "work contract" form (#4107)
* added more budget stats to customer/project/activity detail screen (#4107)
* added background color for weekend in calendar (#4107)
* allow to open edit screen (customer, project, activity) without requirement for the view_X permission (#4107)
* allow zero duration input (set end-time automatically) (#4107)

[2.0.24]
* Update Kimai to 2.0.28
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.0.28)
* fix invalid user preference (outdated route for homepage) automatically (#4172)
* respect line breaks (in timesheet description) in calendar popover (#4172)
* added first-day-of-work handling (not visible in UI yet) (#4172)

[2.0.25]
* Update Kimai to 2.0.29
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.0.29)
* show button title if delete is used in page actions
* fix invoice due date depends on invoice date, replace DateTime with DateTimeI…
* lowercase all font names in PDFs, otherwise they fail loading
* hide empty fieldset (work-contract page)
* activate contract_other_profile by default for admin and super-admin
* deactivate rule to check "maximum duration of entries" by default
* allow to deactivate presets in DateRange Picker (for Devs)

[2.1.0]
* Update Kimai to 2.0.30
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.0.30)
* Translations update from Hosted Weblate (#4210)
* remove text from tag choice after selection - fixes https://github.com/kimai/kimai/issues/4076
* fix timesheet allows to use deactivated activities - fixes https://github.com/kimai/kimai/issues/4229
* upgraded composer dependencies
* upgraded frontend dependencies
* added basic tests for validator

[2.1.1]
* Update Kimai to 2.0.31
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.0.31)
* Translations update from Hosted Weblate (#4252)
* Added supervisor configuration for user (#4251)
* Added weekly-times form shows global form errors (#4233)
* Fix respect timezone in DateTimePicker (for plugin devs)
* Fix time can be optional and null (for plugin devs)
* Allow 64 characters for username (#4199)

[2.1.2]
* Update Kimai to 2.0.32
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.0.32)
* Translations update from Hosted Weblate (#4267)
* Change isWeekend() check to isWorkingDay() and fallback to Saturday/Sunday as weekend (#4261)
* Make sure that accountNumber on user has max length of 30 (for SAML registrations) (#4256)
* Default export PDF layout has more blocks, for easier customization (#4256)
* Calendar support for JSON feeds (#4256)
* Added events and logic to add new calendar sources (#4256)
* Added code to help finding problems for SAML logins with unusual values (#4256)
* Added twig deprecation notice for date_full (#4256)
* Fixed: create form if user cannot select supervisor (#4256)
* Remove constant for default locale (will never change from en) (#4256)

[2.1.3]
* Update Kimai to 2.0.33
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.0.33)
* hide un-defined meta-fields in details view
* Fixes "page out of range" #4279
* document breaking change about DATABASE_URL
* default charset is utf8mb4
* split off upgrade infos for version 1.x

[2.1.4]
* Update Kimai to 2.0.34
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.0.34)
* Fix: a template deprecation (#4281)
* Fix: colors missing in user-chooser in calendar (#4281)
* Translations update from Hosted Weblate (#4282)

[2.1.5]
* Update Kimai to 2.0.35
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.0.35)
* Added: Change settings shortcut card position to be sticky (#4185)
* Fixes: Fatal error on "Date and Time" field (kevinpapst/kimai-plugins#98)
* Fixes: If 5 favorites are defined for in last activities, no further entries are added to the list (#4259)
* Fixes: "Revenue this year" in Dasboard includes Billable hours (#4308)
* Fixes: Dashboard: new widget for billable hours (#3960)
* Fixes: Problem using SSO with Google (#4317)
* Fixes: translucent scroll-border (e.g. duration dropdown)
* Simplify plugin screen (remove context menu for plugins, make name a link to the homepage
* Bump composer dependencies
* Upgraded to Symfony 6.3
* [BC] Plugin authors: Improved code-styles (type-safety, return types)
* [BC] Plugin authors: Refactored widgets and statistic fetching

[2.2.0]
* Update Kimai to 2.1.0
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.1.0)
* Direct link from "Active timesheets" widget icon to filter in "all times"
* Allow to configure that new accounts need to reset their password
* Added wizard to force password reset by user
* Fix "skin" translation in wizard
* Allow to export "single user reports" to Excel
* Allow to replace or append description via timesheet batch update

[2.3.0]
* Update Kimai to 2.2.1
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.2.0)
* Added missing twig filter and functions to InvoiceSecurityPolicy (#4359)
* Enable CSRF for logout (#4359)
* Add parameter types and fix a phpstan issue (#4384)
* Deactivate deprecation logging in prod, as long as so many third party bundles are causing too much noise (#4359)
* Fixed several deprecations (#4359)

[2.4.0]
* Update Kimai to 2.3.0
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.3.0)
* Translations update from Hosted Weblate (#4346)
* Added role cards to permission screen (#4401)
* Move user-preferences to edit submenu (#4412)
* Unify user preferences with other forms (#4412)
* Use light fieldset for all user-edit-forms (#4412)
* Support offcanvas elements (#4412)
* Allow to dynamically inject toolbar buttons (via plugins) (#4412)

[2.5.0]
* Update Kimai to 2.4.1
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.4.1)
* Log improvements (datetime format and reformatted) (#4427)
* Request new password by the user upon next login (#4442)
* Open timesheet edit dialog from export listing (#4427)
* Translations update from Hosted Weblate (#4447)
* LDAP issues - allow to migrate from local to LDAP account (#4445)
* Show button to duplicate old timesheets, even those in lockdown period (#4427)
* Changed export column header (user, username, rate_internal) (#4427)
* LDAP issues - allow to migrate from local to LDAP account (#4445)
* Dashboard tooltip remains in view after removing widget (#4426)
* Dashboard JS error if all widgets were removed (#4442)

[2.5.1]
* Update Kimai to 2.5.0
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.5.0)
* added command to list users (#4454)
* work contract validations (#4454)
* prevent error with missing params (#4454)
* use collapsible boxes for work-contract screen (#4454)
* added classes to target menu buttons in custom rules (#4454)
* bump to fontawesome 6 and replace restart icon (#4454)
* change repeat icon for recent activities (#4454)
* moved user bookmarks (favorites) to top nav (#4454)
* fix totp seconds window (leeway) (#4454)
* new migration to fix remaining user preferences with dots in name (#4454)
* remove duplicate named column in user screen (#4454)
* added missing filter and tags to InvoiceSecurity (#4454)
* use explicit menu service to generate menu (#4454)
* disable webpack notifier, incompatible with mac arm (#4454)
* allow PHP 8.3 (#4471)
* translations updates (#4466)
* fix use of range() and auto-width columns for Excel exports (#4471)

[2.6.0]
* Update Kimai to 2.6.0
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.6.0)
* Sort invoice items by date before passing to template, e.g. do not split expenses and times (#4495)
* New calendar entry title combination for customer, project and activity
* Show not_invoiced and not_exported data in detail screens
* Force logout if user is disabled
* Reduced amount of database queries on several screens
* Open-close status on work-contract screen for users without configuration
* Failsafe order/orderBy in query if manually manipulated to be null
* Unify statistic calculation (not_invoiced and not_exported) across screens
* Bump composer packages, upgrade Symfony to 6.4

[2.7.0]
* Update Kimai to 2.7.0
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.7.0)
* Show API URL for simpler integration in user profile (#4506)
* The create user command allows to set the flag to reset password upon next login (#4506)
* Support setting min and max date on date and daterange pickers (#4506)
* Translations update from Hosted Weblate (#4507)
* Internal Plugin API improvements (#4506)

[2.8.0]
* Update Kimai to 2.8.0
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.8.0)
* Translations update from Hosted Weblate (#4517)
* Fix wrong default protocol for customer homepage (#4514)
* Fix missing data of deactivated users in time-period reports (#4523)
* Fix many deprecations (#4508)
* Bump composer packages (#4508)
* Make calendar serializer compatible with DateTimeImmutable (#4508)
* Release-drafter: Adapt supported PHP versions (#4512)

[2.9.0]
* Update Kimai to 2.9.0
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.9.0)
* Translations update from Hosted Weblate (#4531)
* Started refactoring: use DateTimeImmutable / DateTimeInterface where possible (#4526)
* Fix: TRUSTED_PROXIES setting was ignored (#4533)
* Re-enable Kimai test in docker test build script (#4541) by @Not-Code
* Bump dependencies and fix more deprecations
* Fix a BC break in PHPWord 1.2 (#4526)

[2.9.1]
* Update Kimai to 2.10.0
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.10.0)
* Allow to edit username (#4559)
* Redirect to users chosen locale if current user = edited user in "edit profile" screen (#4549)
* Show currently logged-in user in dropdown, in case it's a system-account (#4549)
* Support for showing and sorting column system account in user listing (#4549)
* Only super-admins can edit their own supervisor (#4549)
* WCAG: add title or aria-label to many links/buttons/dropdowns in the general layout (#4549)
* New command to test emails (#4549)
* Support addon buttons in working time screen (#4549)
* Make 'pagebreak' optional in pdf layout exporter (#4561)
* Fix sorting user dropdown (#4549)
* Support default time on date-time-picker (#4579)
* Translations update from Weblate (#4576, #4551)
* Merged/Removed two translation files (#4572)

[2.9.2]
* Update Kimai to 2.11.0
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.11.0)

[2.9.3]
* Update Kimai to 2.12.0
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.12.0)
* Support half-days for holidays (#4609)
* Fixes redirect loop with Azure SAML (#4609)
* Fix missing locales - e.g. en_US locale configuration may be missing? (#4652)
* Bump composer packages (#4655)
* Added validator for customers timezone (#4655)
* Include user accountNumber in CSV and Excel export (#4655)
* Improve next customer number calculation (#4655)
* Fixes hidden context-menu behind tabler header (#4655)
* Upgraded theme, show top nav links as buttons (#4633)
* Revert to classical bootstrap form layout (#4632)
* Weekly hours improvements: table padding, allow 0 recent activities row, re-use activity favorites (#4631)
* Added project filter in user-list reports (#4615)
* Translations update from Hosted Weblate (#4658) (#4614)

[2.9.4]
* Update Kimai to 2.13.0
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.13.0)
* Translations update from Hosted Weblate (#4693)
* Fixes #4684 - calendar was using request locale instead of user configuration
* Prevent 500 on plugin controller if domain is not reachable
* Prevent error due to missing username
* Fix timezone reporting issue (times were shown on wrong date, if reporter and current user are in different timezones)
* Allow filter customers by country
* Change order of daterange input and select
* Speed up permission checks
* Support in-/decrementor in invoice date number generator
* Make sure current users permissions are respected during timesheet API collection call
* Secure activity/project/customer detail API call
* Rename internal menu IDs
* Cache voter checks
* Bump composer packages

[2.9.5]
* Update Kimai to 2.14.0
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.140)
* Added API tokens, deprecate API passwords (#4637)
* Configurable activity and project number (#4729)
* Add support for negative durations in All Users reports (#4717)
* Show "link has expired message" in password reset screen (#4710)
* Added date objects as hydrator variables - for custom date formats in invoice templates (#4710)
* Utilize UserService for SAML (#4748)
* Split docker to use base image for faster builds (#4586)
* Added logging for invalid SAML responses (#4710)
* Translations update from Hosted Weblate (#4722) (#4746)
* Fix: allow changing locale in DateRangeType
* Fix: show meta-fields with null values (e.g. booleans with false where hidden) (#4686)
* Fix: permission check: allow to remove view_own_timesheet but still record times (#4710)
* Fix: prevent error 500 if customer country is empty (#4710)
* Fix: API 500 error if project does not exist when creating new timesheet (#4710)
* Fix: tags are not created in remote-search mode (#4710)
* Fix: do not "export items" by default (#4339)
* Fix: daterange query, if user an request locale are different (#4710)

[2.9.6]
* Update Kimai to 2.15.0
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.15.0)
* Added `api_access` permission for limiting API access (#4779)
* Added several `_process` dates to invoice hydrator (#4749)
* Added activity.number and project.number to invoice hydrator (#4749)
* Added `activity.invoice_text` and `project.invoice_text` to invoice hydrator (#4749)
* Fix Cannot redirect to empty URL SAML issue (#4761)
* Simplify 2fa (deactivation) form (#4749)
* Clarify expected datetime format in API docs (#4749)
* Allow to order by activity and project number (#4760)
* Do not store hashed password for LDAP and SAML users (#4755)
* API tokens not working with certain Apache configurations (#4774)
* Dashboard widget "my working times" calculation behavior changed (#4749)
* Catch Throwables if `curl_multi_exec` is disabled (#4740)
* Fixed several deprecations (#4749)
* Fix "invoices" internal menu name

[2.10.0]
* Update Kimai to 2.16.0
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.16.0)
* Translations update from Hosted Weblate (#4826) (#4814) (#4812)
* Docker: Fix TLS certificate verification for LDAP authentication (#4802)
* Remove `view_own_timesheet` permission check from Wizard controller
* Adds a string formatter for the general export function
* Adds a twig function to sanitize data against DDE attacks (blame Microsoft not Kimai!)
* Fix just another doctrine deprecation
* Allow to use english format for DateRanges even if user uses different locale
* Added macro to simplify cross-links to search results
* Cross-link "filter timesheets" view from weekly reports
* Activate Taiwanese locale - thanks @IepIweidieng
* Improve `min_day` and `max_day` handling in some date chooser
* Added missing redirect after saving API token
* Make sure that access tokens expiry date is in the future (#4808)
* Allow to delete api tokens for other users (#4809)

[2.10.1]
* Update Kimai to 2.16.1
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.16.1)
* Fix hidden API token (#4828)
* Translations update from Hosted Weblate (#4827)

[2.11.0]
* Update Kimai to 2.17.0
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.17.0)
* Translated using Weblate (#4850, #4862)
* Improve tag handling, fixes #4396
* Fix project duplication causing duplicate project numbers (#4836)
* Trigger a change on updated meta fields to allow recalculation by TimesheetCalculator (#4836)
* Improve API auth, failing under certain cicrumstances (#4836)
* Slow down invalid session detection, to increase security (#4836)
* Bump composer packages (#4836)
* Utilize UserService for creating new accounts (#4836)
* Erase credentials after hashing password and token (#4836)

[2.12.0]
* Update Kimai to 2.18.0
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.18.0)
* Translations update from Hosted Weblate (#4876)
* INVOICE: do not use activity name as fallback for description (#4884)
* improve project end handling in weekly-hours screen
* Translated using Weblate (#4876)
* only trigger `api_access` voter if API call was triggered from outside UI (#4878)

[2.13.0]
* Update Kimai to 2.19.0
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.19.0)
* Raise contrast in dark mode (#4922)
* Allow to switch user on weekly-hours screen (#4922)
* Show yearly date range only if financial year is activated (report form) (#4922)
* Added dropdown to year picker widget (#4922)

[2.13.1]
* Update Kimai to 2.19.1
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.19.1)

[2.14.0]
* Update Kimai to 2.20.1
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.20.1)

[2.15.0]
* Update Kimai to 2.21.0
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.21.0)

[2.16.0]
* Update Kimai to 2.22.0
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.22.0)
* Added support for changeable work contract types (#5069)
* Added API endpoints to fetch invoices (#5070)
* Allow Authorization header in CORS request
* Fixed: star/unstar of favorites failed sometimes (#5068)
* Fixed: Dashboard not loading for users without team (#5043)
* Prepare pagination support in API (#5073)
* Translations update from Hosted Weblate (#5045)
* Added permission `hours_own_profile` for every user (see own working-hours contract and stats) (#5043)

[2.17.0]
* Update Kimai to 2.23.0
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.23.0)
* Removed session search feature (which caused UX problems and raised a few bugs as well)
* Fixed RTL glitches: reverse arrows in RTL and dropdown misalignment
* Password reset flow re-written:
* Added `modified_at` and `created_at` Doctrine behaviors and applied to Customer, Project and Activity
* Simplified core email templates: removed background color and tables
* Simplified wording of "password reset" email
* Improved available columns in "Monthly overview" report: duration, revenue, costs, profit

[2.18.0]
* Update Kimai to 2.24.0
* [Full changelog](https://github.com/kimai/kimai/releases/tag/2.24.0)
* Respect last working day in working times calculation (#​5097)
* Order project specific activities at the begin of the dropdown (#​4674)
* First support for installing plugins via composer (#​5112)
* Ignore files from showing up in a release (#​5111)
* Fix daterange highlight color in dark mode (#​5097)
* Fix pre-selected date format in export dropdown (#​5097)
* Export column alignment (#​5029)
* Fix an invalid namespace (#​5097)
* Translations update from Hosted Weblate (#​5120) (#​5098) (#​5110)

[2.19.0]
* Update kimai to 2.25.0
* [Full Changelog](https://github.com/kimai/kimai/releases/tag/2.25.0)
* Added API endpoints to delete customer/project/activity ([#&#8203;5181](https://github.com/kimai/kimai/issues/5181))
* Always include `begin` and `end` API fields for Admins ([#&#8203;5134](https://github.com/kimai/kimai/issues/5134))
* Improve visibility of batch-update checkboxes  - fixes [#&#8203;5146](https://github.com/kimai/kimai/issues/5146) ([#&#8203;5109](https://github.com/kimai/kimai/issues/5109))
* Improve visibility of `overlapping` indicator  - fixes [#&#8203;5147](https://github.com/kimai/kimai/issues/5147) ([#&#8203;5109](https://github.com/kimai/kimai/issues/5109))

[2.20.0]
* Update kimai to 2.26.0
* [Full Changelog](https://github.com/kimai/kimai/releases/tag/2.26.0)
* Fix missing SAML redirect after login ([#&#8203;5189](https://github.com/kimai/kimai/issues/5189))
* Bring back two deprecated methods (leading to issue in some plugins) ([#&#8203;5189](https://github.com/kimai/kimai/issues/5189))
* API BC: throw if all requested tags were not found  - fixes [#&#8203;4792](https://github.com/kimai/kimai/issues/4792) ([#&#8203;5189](https://github.com/kimai/kimai/issues/5189))
* Translations update from Hosted Weblate ([#&#8203;5192](https://github.com/kimai/kimai/issues/5192))
* Composer bundle updates ([#&#8203;5189](https://github.com/kimai/kimai/issues/5189))

[2.20.1]
* Symlink export path

[2.20.2]
* Revert "invoices is gone upstream, seems to be inside data"

[2.21.0]
* Update kimai to 2.27.0
* [Full Changelog](https://github.com/kimai/kimai/releases/tag/2.27.0)
* Faster (spreadsheet) exporter and other Export improvements ([#&#8203;5238](https://github.com/kimai/kimai/issues/5238))
* Show tags in export listing ([#&#8203;4603](https://github.com/kimai/kimai/issues/4603))
* Show project number in Excel export ([#&#8203;4891](https://github.com/kimai/kimai/issues/4891))
* Show numbers without currencies in CSV export ([#&#8203;5149](https://github.com/kimai/kimai/issues/5149))
* Export via timesheet listing without search form ([#&#8203;5234](https://github.com/kimai/kimai/issues/5234))
* Fix rejection and approval message ([#&#8203;5212](https://github.com/kimai/kimai/issues/5212))
* Translations update from Hosted Weblate ([#&#8203;5211](https://github.com/kimai/kimai/issues/5211))
* Fix pagination on datatables with arrays (fixed: missing results if array had more than 50 entries) ([#&#8203;5212](https://github.com/kimai/kimai/issues/5212))
* Upgrade tests to PhpUnit 10 ([#&#8203;5252](https://github.com/kimai/kimai/issues/5252))
* Fixed unused form options ([#&#8203;5220](https://github.com/kimai/kimai/issues/5220))
* More customization options for forms ([#&#8203;5212](https://github.com/kimai/kimai/issues/5212))
* Fix: toolbar causes sandbox issues in `dev` environment with HTML invoice ([#&#8203;5212](https://github.com/kimai/kimai/issues/5212))
* Fix frontend builds after fresh clone + bump all dependencies ([#&#8203;5210](https://github.com/kimai/kimai/issues/5210))
* Add php 8.3 to issue template ([#&#8203;5246](https://github.com/kimai/kimai/issues/5246))

[2.22.0]
* Update kimai to 2.28.0
* [Full Changelog](https://github.com/kimai/kimai/releases/tag/2.28.0)
* Fixes "Wrong Year on Dashboard" ([#&#8203;5276](https://github.com/kimai/kimai/issues/5276)) ([#&#8203;5253](https://github.com/kimai/kimai/issues/5253))
* Fixes new CSV exporter includes BOM ([#&#8203;5253](https://github.com/kimai/kimai/issues/5253))
* Fixes "This is an auto generated e-mail" twice in email ([#&#8203;5304](https://github.com/kimai/kimai/issues/5304)) ([#&#8203;5253](https://github.com/kimai/kimai/issues/5253))
* Fixes date column in export ([#&#8203;5308](https://github.com/kimai/kimai/issues/5308))
* Fixes usage of activity api globals parameter  ([#&#8203;5284](https://github.com/kimai/kimai/issues/5284))
* Translations update from Hosted Weblate ([#&#8203;5293](https://github.com/kimai/kimai/issues/5293)) ([#&#8203;5254](https://github.com/kimai/kimai/issues/5254))
* Dev: JS API for batch actions ([#&#8203;5253](https://github.com/kimai/kimai/issues/5253))

[2.23.0]
* Update kimai to 2.29.0
* [Full Changelog](https://github.com/kimai/kimai/releases/tag/2.29.0)
* Improved daterange-picker selections: added last months and last quarters ([#&#8203;5317](https://github.com/kimai/kimai/issues/5317))
* Improved export column lengths ([#&#8203;5325](https://github.com/kimai/kimai/issues/5325))
* Added logout button to "Remember me" login form ([#&#8203;5325](https://github.com/kimai/kimai/issues/5325))
* Year-selector: added dropdown with the list of the last 5 years ([#&#8203;5325](https://github.com/kimai/kimai/issues/5325))
* Month selector: added dropdown with the list of all months up to January -2 years ([#&#8203;5325](https://github.com/kimai/kimai/issues/5325))
* Week selector:  ([#&#8203;5325](https://github.com/kimai/kimai/issues/5325))
* Added new variable to invoice hydrator to detect if entry is a fixed rate ([#&#8203;5325](https://github.com/kimai/kimai/issues/5325))
* Translations update from Hosted Weblate ([#&#8203;5342](https://github.com/kimai/kimai/issues/5342)) ([#&#8203;5318](https://github.com/kimai/kimai/issues/5318)) ([#&#8203;5306](https://github.com/kimai/kimai/issues/5306))
* Fixed SQL quotes for use in migration with MySQLs ANSI_MODE ([#&#8203;5325](https://github.com/kimai/kimai/issues/5325))
* Fix `z-index` issue with sticky table header and dropdown ([#&#8203;5325](https://github.com/kimai/kimai/issues/5325))
* Cleanup several translations ([#&#8203;5325](https://github.com/kimai/kimai/issues/5325))

[2.24.0]
* Update kimai to 2.30.0
* [Full Changelog](https://github.com/kimai/kimai/releases/tag/2.30.0)
* Added missing InvoiceTemplate field (`company`, `title`) validator ([#&#8203;5345](https://github.com/kimai/kimai/issues/5345))
* Graceful fallback for missing working-contract mode ([#&#8203;5345](https://github.com/kimai/kimai/issues/5345))
* Improve email test command (use configured MAIL_FROM) ([#&#8203;5345](https://github.com/kimai/kimai/issues/5345))
* Additional form types for simple usage in SystemConfiguration and UserPreferences ([#&#8203;5345](https://github.com/kimai/kimai/issues/5345))
* Allow to extend the working time query via event ([#&#8203;5345](https://github.com/kimai/kimai/issues/5345))
* Fix export dates are not localized ([#&#8203;5368](https://github.com/kimai/kimai/issues/5368))
* Prepare break time field ([#&#8203;5366](https://github.com/kimai/kimai/issues/5366))
* Translation cleanup ([#&#8203;5370](https://github.com/kimai/kimai/issues/5370))
* Translations update from Hosted Weblate ([#&#8203;5344](https://github.com/kimai/kimai/issues/5344)) ([#&#8203;5369](https://github.com/kimai/kimai/issues/5369)) ([#&#8203;5362](https://github.com/kimai/kimai/issues/5362))

